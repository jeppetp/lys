let mic, fft;
var width = window.innerWidth
var height = window.innerHeight
var amplitude


var tincrease = 0.01
var t = 0;
var speclen = 256

function windowResized() {
  width = windowWidth
  height = windowHeight
  resizeCanvas(width, height)
  background(0,0,0)
}

function preload(){
  sound = loadSound('assets/teknotest.wav');
}

function setup(){
  smooth();
  let cnv = createCanvas(window.innerWidth, window.innerHeight, WEBGL);
  cnv.mouseClicked(togglePlay);
  fft = new p5.FFT();
  fft.smooth(0)
  sound.amp(0.2);
  colorMode(HSB, 255);
  angleMode(RADIANS);
  background(0,0,0);
  windowResized();
}

function pause() {
  if (tincrease){
    tincrease = 0
  }
  else{
    tincrease = 0.01
  }
}

function togglePlay() {
  if (sound.isPlaying()) {
    sound.pause();
  } else {
    sound.loop();
  }
}

function draw() {
  //get fft data
  let spectrum = fft.analyze(speclen);


  background(0,0,0);

  for(let i = 0; i < speclen; i++){
    push()
    translate(cos(t + 2*i*(PI/speclen))*0.3*min(height,width) * (1 + 0.01*((i**2)/(speclen**2))*spectrum[i+1]), sin(t+2*i*(PI/speclen))*0.3*min(height,width) * (1 + 0.005*((i**2)/(speclen**2))*spectrum[i+1]));
    rotateZ(t + 2*i*(PI/speclen))
    rotateY(t + i*(PI/speclen)/2 + 0.05*((i**2)/(speclen**2))*spectrum[i+1] + t*0.5)

    stroke(((i/speclen)*256)%256,255,0)
    noStroke()
    fill(((i/speclen)*256)%256,255,255)

    strokeWeight(1)

    box(min(width,height)/5, min(width,height)/5,min(width,height)/5);
    pop()
  }

  t+=tincrease;
}
