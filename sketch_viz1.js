let mic, fft;
var width = window.innerWidth
var height = window.innerHeight
var amplitude
var tincrease = 0.01

function windowResized() {
  width = windowWidth
  height = windowHeight
  resizeCanvas(width, height)
  background(0,0,0)
}

function preload(){
  sound = loadSound('assets/teknotest.wav');
}

function setup(){
  smooth();
  let cnv = createCanvas(window.innerWidth, window.innerHeight, WEBGL);
  cnv.mouseClicked(togglePlay);
  mic = new p5.AudioIn();
  mic.start();
  fft = new p5.FFT();
  // fft.setInput(mic);
  fft.smooth(0.1)
  sound.amp(0.2);
  colorMode(HSB, 255);
  angleMode(RADIANS);
  background(0,0,0);
  windowResized();
}

function pause() {
  if (tincrease == 0){
    tincrease = 0.01
  }
  else{
    tincrease = 0
  }
}

function togglePlay() {
  if (sound.isPlaying()) {
    sound.pause();
  } else {
    sound.loop();
  }
}

var t = 0;
var huee = 0;

var speclen = 512
var spec = []*512

function draw() {
  blendMode(EXCLUSION)
  noiseDetail(4, 0.5)
  let spectrum = fft.analyze(speclen);
  // let energy = fft.getEnergy(20, 90);
  // fill(255, 255, 255)
  //setup
  background(0,0,0);
  //strokeWeight(2)
  //camera angle

  //draw
  for(let i = 0; i < speclen; i++){
    push()
    translate(cos(t + 2*i*(PI/speclen))*0.3*min(height,width), sin(t+2*i*(PI/speclen))*0.3*min(height,width));
    rotateZ(t + 2*i*(PI/speclen))
    rotateY(t + 3*i*(PI/speclen)/2 + 0.05*((i**2)/(speclen**2))*spectrum[i+1])
    // fill(0,0,255);
    stroke(((i/speclen)*256)%256,255,0)
    noStroke()
    fill(((i/speclen)*256)%256,255,255)

    strokeWeight(1)
    // fill(0,0,0)
    // noFill()
    // normalMaterial();
    rectMode(CORNERS)
    // plane(((i**2)/(speclen**2))*spectrum[i+1]*0.1+30)
    box(min(width,height)/5, min(width,height)/5,min(width,height)/5);
    pop()
  }

  //time increase
  t+=tincrease;
}
